package com.EJV.restController;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.EJV.model.Categorie;
import com.EJV.model.Jeux;
import com.EJV.service.IJeuxService;

@RestController
@CrossOrigin("*")
@RequestMapping("/rest/jeux")
public class JeuxRestController {

	@Autowired
	private IJeuxService jeuxService;

	@GetMapping("/getJeux")
	public Page<Jeux> getAllJeux(int page, int size) {
		return jeuxService.getAllJeux(page, size);
	}

	@GetMapping("/getLastJeux")
	public List<Jeux> getLastJeux() {
		return jeuxService.getLastJeux();
	}

	@GetMapping("/getTopJeux")
	public List<Jeux> getTopJeux() {
		return jeuxService.getTopJeux();
	}

	@GetMapping("/getJeuxByNom")
	public Page<Jeux> getJeuxByNom(int page, int size, String nom) {
		return jeuxService.chercherJeux(page, size, nom);
	}

	@GetMapping("/getJeuxByIdCategorie")
	public Page<Jeux> getJeuxByidCategorie(int page, int size, Long idCategorie) {
		return jeuxService.chercherJeuxByCategorie(page, size, idCategorie);
	}

	@GetMapping("/getjeuxById")
	public Jeux getJeuxById(@RequestParam("idJeux") Long idJeux) {
		return jeuxService.getJeuxById(idJeux);
	}

	@PostMapping("/addGame")
	public Jeux addJeu(@RequestParam("titre") String nom) {
		Jeux jeu = jeuxService.addJeu(nom, "desc ", 120, "img/games/1.jpg", "", "", "http://youtube.com", new Date(),
				new Categorie("Sport"), 5);
		return jeu;

	}
}
