package com.EJV.service;

import java.util.Date;
import java.util.List;

import org.springframework.data.domain.Page;

import com.EJV.model.Categorie;
import com.EJV.model.Jeux;

public interface IJeuxService {

	public Page<Jeux> getAllJeux(int page, int size);

	public Jeux getJeuxById(Long idJeux);

	public Jeux addJeu(String nom, String description, double prix, String urlImage, String urlImageBar,
			String urlImageBlockMd, String urlVideo, Date dateSortie, Categorie Categorie, int avis);

	public List<Jeux> getLastJeux();

	public List<Jeux> getTopJeux();
	
	public Page<Jeux> chercherJeux(int page, int size, String nom);
	
	public Page<Jeux> chercherJeuxByCategorie(int page, int size, Long idCategorie);

}
