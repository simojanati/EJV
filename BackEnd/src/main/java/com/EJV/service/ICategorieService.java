package com.EJV.service;

import java.util.List;

import com.EJV.model.Categorie;

public interface ICategorieService {

	public List<Categorie> getAllCategorie();

}
