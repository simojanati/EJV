package com.EJV.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by simoj on 20/05/2017.
 */

@Entity
public class Jeux implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idJeux;

	@Column(name = "nom")
	private String nom;

	@Column(name = "description")
	private String description;

	@Column(name = "prix")
	private double prix;

	@Column(name = "avis")
	private int avis;

	@Column(name = "url_video")
	private String urlVideo;

	@Column(name = "url_image")
	private String urlImage;

	@Column(name = "url_image_bar")
	private String urlImageBar;

	@Column(name = "url_image_block_md")
	private String urlImageBlockMd;

	@Temporal(TemporalType.DATE)
	@Column(name = "date_sortie")
	private Date dateSortie;

	@ManyToOne
	@JoinColumn(name = "id_categorie", referencedColumnName = "id_categorie")
	private Categorie categorie;

	public Long getIdJeux() {
		return idJeux;
	}

	public void setIdJeux(Long idJeux) {
		this.idJeux = idJeux;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getAvis() {
		return avis;
	}

	public void setAvis(int avis) {
		this.avis = avis;
	}

	public String getDescription() {
		return description;
	}

	public String getUrlImageBar() {
		return urlImageBar;
	}

	public void setUrlImageBar(String urlImageBar) {
		this.urlImageBar = urlImageBar;
	}

	public String getUrlImageBlockMd() {
		return urlImageBlockMd;
	}

	public void setUrlImageBlockMd(String urlImageBlockMd) {
		this.urlImageBlockMd = urlImageBlockMd;
	}

	public Date getDateSortie() {
		return dateSortie;
	}

	public void setDateSortie(Date dateSortie) {
		this.dateSortie = dateSortie;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getPrix() {
		return prix;
	}

	public void setPrix(double prix) {
		this.prix = prix;
	}

	public String getUrlVideo() {
		return urlVideo;
	}

	public void setUrlVideo(String urlVideo) {
		this.urlVideo = urlVideo;
	}

	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

	public Categorie getCategorie() {
		return categorie;
	}

	public void setCategorie(Categorie categorie) {
		this.categorie = categorie;
	}

	public Jeux(String nom, String description, double prix, String urlVideo, String urlImage, String urlImageBar,
			String urlImageBlockMd, Date dateSortie, Categorie categorie, int avis) {
		super();
		this.nom = nom;
		this.description = description;
		this.prix = prix;
		this.urlVideo = urlVideo;
		this.urlImage = urlImage;
		this.urlImageBar = urlImageBar;
		this.urlImageBlockMd = urlImageBlockMd;
		this.dateSortie = dateSortie;
		this.categorie = categorie;
		this.avis = avis;
	}

	public Jeux() {
		super();
		// TODO Auto-generated constructor stub
	}

}
