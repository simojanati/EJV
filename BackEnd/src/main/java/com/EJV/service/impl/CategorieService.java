package com.EJV.service.impl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.EJV.dao.ICategorieRepository;
import com.EJV.model.Categorie;
import com.EJV.service.ICategorieService;

@Service
@Transactional
public class CategorieService implements ICategorieService {

	@Autowired
	private ICategorieRepository categorieRepository;

	@Override
	public List<Categorie> getAllCategorie() {
		return categorieRepository.findAll();
	}

}
