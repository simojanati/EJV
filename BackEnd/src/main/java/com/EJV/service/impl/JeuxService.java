package com.EJV.service.impl;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.EJV.dao.IJeuxRepository;
import com.EJV.model.Categorie;
import com.EJV.model.Jeux;
import com.EJV.service.IJeuxService;

@Service
@Transactional
public class JeuxService implements IJeuxService {

	@Autowired
	private IJeuxRepository jeuxRepository;

	@Override
	public Page<Jeux> getAllJeux(int page, int size) {
		return jeuxRepository.findAll(new PageRequest(page, size));
	}

	@Override
	public Jeux getJeuxById(Long idJeux) {
		return jeuxRepository.findOne(idJeux);
	}

	@Override
	public Jeux addJeu(String nom, String description, double prix, String urlImage, String urlImageBar,
			String urlImageBlockMd, String urlVideo, Date dateSortie, Categorie Categorie, int avis) {
		Jeux jeu = new Jeux(nom, description, prix, urlImage, urlImageBar, urlImageBlockMd, urlVideo, dateSortie,
				Categorie, avis);
		return jeuxRepository.save(jeu);
	}

	@Override
	public List<Jeux> getLastJeux() {
		return jeuxRepository.getLastjeux();
	}

	@Override
	public List<Jeux> getTopJeux() {
		return jeuxRepository.getTopjeux();
	}

	@Override
	public Page<Jeux> chercherJeux(int page, int size, String nom) {
		return jeuxRepository.chercherJeux(nom, new PageRequest(page, size));
	}

	@Override
	public Page<Jeux> chercherJeuxByCategorie(int page, int size, Long idCategorie) {
		return jeuxRepository.chercherJeuxByCategorie(idCategorie, new PageRequest(page, size));
	}
}
