package com.EJV;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.domain.Page;

import com.EJV.model.Jeux;
import com.EJV.service.IJeuxService;

@SpringBootApplication
public class EjvApplication implements CommandLineRunner {

	@Autowired
	private IJeuxService jeuxService;

	public static void main(String[] args) {
		SpringApplication.run(EjvApplication.class, args);
	}

	@Override
	public void run(String... arg0) throws Exception {

		Page<Jeux> pages = jeuxService.chercherJeuxByCategorie(0, 3, 1L);

		for (Jeux jeux : pages) {
			System.out.println(jeux.getNom());
		}

	}
}
