var urlAllJeux = 'http://localhost:8080/rest/jeux/';
var urlAllCategories = 'http://localhost:8080/rest/categorie/';

var app = angular.module("M2-Miage", [ "ngRoute" ]);

app.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/games', {
		templateUrl : 'games.html',
		controller : 'allJeux'
	}).when('/home', {
		templateUrl : 'home-magazine.html',
		controller : 'allIndexJeux'
	}).when('/contact', {
		templateUrl : 'contact.html'
	}).when('/login', {
		templateUrl : 'login.html'
	}).when('/inscription', {
		templateUrl : 'register.html'
	}).otherwise({
		redirectTo : '/home'
	});
} ]);

app.controller('allJeux', function($scope, $http) {
	$scope.pageJeux = [];
	$scope.categories = [];
	$scope.pageCourant = 0;
	$scope.size = 3;
	$scope.pages = [];

	$http.get(
			urlAllJeux + 'getJeux?page=' + $scope.pageCourant + '&size='
					+ $scope.size).success(function(data) {
		$scope.pageJeux = data;
		$scope.pages = new Array(data.totalPages);
	}).error(function(data) {
		console.log(data);
	});

	$http.get(urlAllCategories + 'getCategories').success(function(data) {
		$scope.categories = data;
	}).error(function(data) {
		console.log(data);
	});

	

	$scope.chercherUnjeux = function(nom) {
		if (event.keyCode == 13) {
			$http.get(
					urlAllJeux + 'getJeuxByNom?page=' + $scope.pageCourant
							+ '&size=' + $scope.size + '&nom=' + nom).success(
					function(data) {
						$scope.pageJeux = data;
						$scope.pages = new Array(data.totalPages);
					}).error(function(data) {
				console.log(data);
			});
		}
	}

	$scope.afficherLesjeux = function() {
		$http.get(
				urlAllJeux + 'getJeux?page=' + $scope.pageCourant + '&size='
						+ $scope.size).success(function(data) {
			$scope.pageJeux = data;
			$scope.pages = new Array(data.totalPages);
		}).error(function(data) {
			console.log(data);
		});
	}
	$scope.goToPage = function(p) {
		$scope.pageCourant = p;
		$scope.afficherLesjeux();
	}

	$scope.chercher = function(idCategorie) {
		console.log(idCategorie);
		$http.get(
				urlAllJeux + 'getJeuxByIdCategorie?page=' + $scope.pageCourant
						+ '&size=' + $scope.size + '&idCategorie='
						+ idCategorie).success(function(data) {
			$scope.pageJeux = data;
			$scope.pages = new Array(data.totalPages);
		}).error(function(data) {
			console.log(data);
		});
	}
});

app.controller('allIndexJeux', function($scope, $http) {
	$scope.lastJeux = [];
	$scope.topJeux = [];

	$http.get(urlAllJeux + 'getLastJeux').success(function(data) {
		$scope.lastJeux = data;
	}).error(function(data) {
		console.log(data);
	});

	$http.get(urlAllJeux + 'getTopJeux').success(function(data) {
		$scope.topJeux = data;
	}).error(function(data) {
		console.log(data);
	});

});