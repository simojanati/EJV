package com.EJV.restController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.EJV.model.Categorie;
import com.EJV.service.ICategorieService;

@RestController
@CrossOrigin("*")
@RequestMapping("/rest/categorie")
public class CategorieRestController {

	@Autowired
	private ICategorieService categorieService;

	@GetMapping("/getCategories")
	public List<Categorie> getAllCategorie() {
		return categorieService.getAllCategorie();
	}
}
