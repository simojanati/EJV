package com.EJV.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.EJV.model.Categorie;

public interface ICategorieRepository extends JpaRepository<Categorie, Long> {

}
