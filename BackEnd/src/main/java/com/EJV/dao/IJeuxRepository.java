package com.EJV.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.EJV.model.Jeux;

@RepositoryRestResource
public interface IJeuxRepository extends JpaRepository<Jeux, Long> {

	@Query(nativeQuery = true, value = "SELECT * from jeux order by date_sortie DESC LIMIT 3")
	public List<Jeux> getLastjeux();

	@Query(nativeQuery = true, value = "SELECT * from jeux order by avis DESC LIMIT 5")
	public List<Jeux> getTopjeux();

	@Query("SELECT j from Jeux j where j.nom like %:x%")
	public Page<Jeux> chercherJeux(@Param("x") String nom, Pageable pageable);

	@Query("SELECT j from Jeux j where j.categorie.idCategorie=:x")
	public Page<Jeux> chercherJeuxByCategorie(@Param("x") Long idCategorie, Pageable pageable);
}
